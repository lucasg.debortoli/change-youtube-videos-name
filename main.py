import logging
from Youtube.API import WrapperAPI


if __name__ == "__main__":
    
    logging.basicConfig(
        format='%(levelname)s: %(message)s',
        encoding='utf-8',
        level=logging.DEBUG
    )
    
    playlist_name = "Angry Birds"
    video_title_elimination = "Playthrough"
    video_title_replace = "Walkthrough"

    api = WrapperAPI()

    # Uses exact match, too lazy to implement levenstein
    playlist_id = api.search_playlist_id(playlist_name)[0]
    
    # Use playlist id to obtain the videos
    videos = api.obtain_videos_in_playlist(playlist_id, video_title_elimination)
    
    api.replace_all_video_names(videos, video_title_elimination, video_title_replace)
