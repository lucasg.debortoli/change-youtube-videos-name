import logging, time, yaml, requests

class WrapperAPI:

    def __init__(self):
        self.api_url = "https://youtube.googleapis.com/youtube/v3"
        self.oauth2_url = "https://accounts.google.com/o/oauth2"
        self.oauth2_path = "auth"
        self.token_path = "token"
        self.videos_path = "videos"
        self.credentials_file = "Credentials/key.yml"
        self.max_retry_bad_request = 3

        self.__load_yaml_data()

        if not (self.token_code.strip()):
            self.__initialize_oauth2()
            body_access_token = self.__body_token_code()
        else:
            body_access_token = self.__body_token_refresh()

        self.__obtain_new_oauth_access_token(body_access_token)
        logging.info(f"Access token: {self.access_token}")

    # Returns a dict of title_playlist (string) -> playlist (dict)
    def search_playlist_id(self, title_match):
        logging.info(f"Searching playlist with name {title_match}\n")

        return self.__navigate_pagination(
            path="playlists",
            parent_id_arg={ "channelId": self.channelId },
            method_filter=self.__update_results_playlist_search,
            filter_kwargs=title_match.lower().strip(),
            result_parser=self.__id_and_title_playlists
        )

    def obtain_videos_in_playlist(self, playlist_id, title_video_match):
        logging.info(f"Searching videos that contains {title_video_match}\n")

        return self.__navigate_pagination(
            path="playlistItems",
            parent_id_arg={ "playlistId": playlist_id },
            method_filter=self.__update_results_videos_in_playlist,
            filter_kwargs=title_video_match.lower().strip(),
            result_parser=self.__id_and_title_videos
        )

    def replace_all_video_names(self, videos, part_to_replace, replacement):
        total = len(videos)
        per_query = 50
        iterations = total // per_query + (0 if total % per_query == 0 else 1)
            
        for i in range(iterations):
            videos_query = videos[i*per_query:(i+1)*per_query]
            videos_id = ",".join(videos_query)
            videos_with_snippet = self.__get_videos(videos_id)

            for video in videos_with_snippet['items']:
                video_id = video["id"]
                new_snippet = video["snippet"]

                new_title = new_snippet["title"].replace(part_to_replace, replacement)        
                new_snippet["title"] = new_title

                resp = self.replace_video_title(video_id, new_snippet)
                logging.info(f"Video {new_title} updated")

    def replace_video_title(self, video_id, new_snippet):
        params = { "part": "snippet", "key": self.api_key }
        headers = {
            "Authorization": f"Bearer {self.access_token}",
            "Accept": "application/json",
            "Content-type": "application-json" 
        }
        request_body = {
            "id": video_id,
            "snippet": new_snippet
        }

        return self.__send_request(
            method="PUT",
            url=f"{self.api_url}/{self.videos_path}",
            params=params,
            headers=headers,
            json=request_body
        )

    def __load_yaml_data(self):
        with open(self.credentials_file) as file:
            yaml_content = yaml.load(file, Loader=yaml.BaseLoader)

        self.api_key = yaml_content["api_key"]

        self.token_code = yaml_content["token_code"]
        self.refresh_token = yaml_content["refresh_token"]
        self.oauth2_client_id = yaml_content["oauth2_client_id"]
        self.oauth2_client_secret = yaml_content["oauth2_client_secret"]
        
        self.channelId = yaml_content["channelId"]

    def __initialize_oauth2(self):
        full_oauth2_url = (
            f"{self.oauth2_url}/{self.oauth2_path}?"
            f"client_id={self.oauth2_client_id}&"
             "redirect_uri=urn:ietf:wg:oauth:2.0:oob&"
             "response_type=code&"
             "scope=https://www.googleapis.com/auth/youtube"
        )

        print(
            "Please go to the next url, follow the steps, paste the token "
            "here and press enter\n" + full_oauth2_url
        )
        
        token_code = input()
        logging.info("Token obtained")
        self.__update_oauth2_key_in_yml("token_code", token_code)
        self.token_code = token_code

    def __obtain_new_oauth_access_token(self, content_request):    
        headers = { "Content-type": "application/x-www-form-urlencoded" }

        access_token_data = self.__send_request(
            method="POST",
            url=f"{self.oauth2_url}/{self.token_path}",
            headers=headers,
            data=content_request            
        )

        if "refresh_token" in access_token_data:
            refresh_token = access_token_data["refresh_token"]
            self.__update_oauth2_key_in_yml("refresh_token", refresh_token)
            self.refresh_token = refresh_token

        self.access_token = access_token_data["access_token"]

    def __body_token_code(self):
        return (
            f"code={self.token_code}&"
            f"client_id={self.oauth2_client_id}&"
            f"client_secret={self.oauth2_client_secret}&"
             "redirect_uri=urn:ietf:wg:oauth:2.0:oob&"
             "grant_type=authorization_code&"
             "scope=https://www.googleapis.com/auth/youtube"
        )

    def __body_token_refresh(self):
        return (
            f"client_id={self.oauth2_client_id}&"
            f"client_secret={self.oauth2_client_secret}&"
            f"refresh_token={self.refresh_token}&"
             "grant_type=refresh_token"
        )

    def __update_oauth2_key_in_yml(self, key, value):
        with open(self.credentials_file) as file:
            yaml_content = yaml.safe_load(file)

        yaml_content[key] = value

        with open(self.credentials_file, 'w') as file:
            yaml.dump(yaml_content, file)

    def __navigate_pagination(self, path, parent_id_arg, method_filter, filter_kwargs, result_parser):
        logging.info(f"Searching {path}\n")

        response = self.__get_set_of_elements(path, parent_id_arg)
        page_info = response["pageInfo"]
        
        total = page_info["totalResults"]
        per_page = page_info["resultsPerPage"]
        elements = "element" + ("" if per_page == 1 else "s") 
        pages = total // per_page + (0 if total % per_page == 0 else 1)

        logging.info((
            f"Total amount: {total}\n"
            f"Per page: {per_page}\n"
            f"Calculated pages: {pages}\n"
            f"Starting to analize results of {path}"
        ))

        results = { "elements": [], "acumulator_params": {} }

        # Iterate through pages
        for page in range(pages):
            items = response["items"]
            logging.info(f"Iterate page {page + 1}: {per_page} {elements}")

            # Iterate through
            for item in items:
                method_filter(results, item, filter_kwargs)

            # Obtain the next page
            if page < pages - 1:
                response = self.__get_set_of_elements(
                    path,
                    parent_id_arg,
                    page_token=response["nextPageToken"]
                )

        return result_parser(results["elements"])


    def __get_set_of_elements(self, path, parent_id_arg, page_token=None):
        query_params = { "part": "id,snippet", "maxResults": 50, "key": self.api_key }
        query_params.update(parent_id_arg)

        headers = { "Content-type": "application-json" }

        if page_token is not None:
            query_params["pageToken"] = page_token
        else:
            logging.info("Page token is none")

        return self.__send_request(
            method="GET",
            url=f"{self.api_url}/{path}",
            params=query_params,
            headers=headers
        )

    def __get_videos(self, videos_list):
        query_params = {
            "part": "id,snippet",
            "maxResults": 50,
            "key": self.api_key,
            "id": videos_list
        }

        headers = { "Content-type": "application-json" }

        return self.__send_request(
            method="GET",
            url=f"{self.api_url}/{self.videos_path}",
            params=query_params,
            headers=headers
        )

    def __id_and_title_playlists(self, items):
        return [item["id"] for item in items]

    def __id_and_title_videos(self, items):
        return [item["snippet"]["resourceId"]["videoId"] for item in items]

    def __update_results_playlist_search(self, results, playlist, title_match):
        title = playlist["snippet"]["title"].strip()
        logging.info(f"Analizing playlist with title: {title}")

        index = title.lower().find(title_match) 
        if index < 0: return     

        # If the title of the playlist contains the search term
        logging.info("Playlist founded!")

        extra_chars = len(title) - len(title_match)
        current_acum = results["acumulator_params"]
        current_extra_chars = current_acum["extra_chars"] if "extra_chars" in current_acum else -1
        current_start_match = current_acum["start_match"] if "start_match" in current_acum else -1

        # Reemplazo la lista de matcheos si es el primer matcheo, 
        # si tiene menos cantidad de letras extra que los matcheos anteriores
        # o si tiene igual cantidad pero el matcheo comienza antes
        if(not current_acum or extra_chars < current_extra_chars or
          (extra_chars == current_extra_chars and index < current_start_match)):

            logging.info("Playlist with better match level at the moment")
            results["elements"] = [playlist]
            results["acumulator_params"]["extra_chars"] = extra_chars
            results["acumulator_params"]["start_match"] = index
            return

        # Si el nivel de matcheo coincide con el actual, agrego este resultado también
        elif extra_chars == current_extra_chars and index == current_start_match:
            logging.info("Playlist with the same match level")
            results["elements"].append(playlist)
            return

        else:
            logging.info("The playlist has not matched enough")

        return

    def __update_results_videos_in_playlist(self, results, video, title_match):
        title = video["snippet"]["title"].strip()
        logging.info(f"Analizing video with title: {title}")
        
        if not title_match in title.lower(): return
        logging.info("Title matched")
        
        results["elements"].append(video)
        return

    def __send_request(self, method, url, **kwargs):

        for i in range(self.max_retry_bad_request):
            response = requests.request(method=method, url=url, **kwargs)
            status_code = response.status_code
            result = self.__get_response_result(response)
            
            retry_request = False

            if not 200 <= status_code <= 299:
                self.__log_bad_response(response, result)
                if 400 <= status_code <= 499:
                    retry_request = self.__try_resolve_client_err(status_code, result)
    
            if not retry_request:
                response.raise_for_status()
                return result
            else:
                logging.info("Retrying to send the same request after handle the error")


        logging.error("Max amount of retries sending requests reached")
        response.raise_for_status()
            
    def __try_resolve_client_err(self, status_code, result):
        resolved = False

        if ((status_code == 400) and (type(result) is dict) and ('error' in result) and
            (result['error'] == 'invalid_grant') and ('error_description' in result)):

            if (result['error_description'] == 'Bad Request'):
                self.__obtain_new_oauth_access_token(self.__body_token_refresh())
                return True

            if (result['error_description'] == 'Token has been expired or revoked.'):
                self.__initialize_oauth2()
                self.__obtain_new_oauth_access_token(self.__body_token_code())
                return True

        if ((status_code == 403) and (type(result) is dict) and ('error' in result) and
            ('errors' in result['error']) and result['error']['errors'][0]['domain'] == 'youtube.quota'):
            logging.error("QUOTA EXCEDED, PLEASE RETRY TOMORROW")

        return False

    def __get_response_result(self, response):
        try:
            return response.json()
        except Exception as e:
            return response.text

    def __log_bad_response(self, response, result):
        logging.error((
            f"Status code: {response.status_code}\n"
            f"Response result: {result}\n"
            f"URL: {response.request.url}\n"
            f"Body: {response.request.body}\n"
            f"Headers: {response.request.headers}"
        ))